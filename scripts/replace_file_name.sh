#!/bin/sh

count=0
for filename in $PWD/*.wav; do 
    DATE=$(date +%Y_%m_%d_%H:%M:%S)
    echo "Current renaming file: $filename"
    mv "$filename" "audiofile_${DATE}_${count}.wav"
    count=`expr $count + 1`
    sleep 2s;
done
