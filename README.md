# bank-project

Basic File Input

<pre>
.
├── ansible
│   ├── README.md
│   └── roles
│       └── database
│           ├── defaults
│           │   └── main.yml
│           ├── files
│           ├── handlers
│           │   └── main.yml
│           ├── meta
│           │   └── main.yml
│           ├── README.md
│           ├── tasks
│           │   ├── 00-initial-setup.yml
│           │   ├── 01-install-pg.yml
│           │   ├── 02-bootup-pg-start.yml
│           │   ├── 03-netowrk-pg.yml
│           │   ├── 04-firewall-pg.yml
│           │   ├── 05-dump-pg.yml
│           │   ├── firewall-pg.yml
│           │   └── main.yml
│           ├── templates
│           ├── tests
│           │   ├── inventory
│           │   ├── pg_db.yml
│           │   └── test.yml
│           └── vars
│               └── main.yml
├── docker-project
│   ├── elk-filebeat
│   │   ├── docker-compose.yml
│   │   └── filebeat
│   │       ├── Dockerfile
│   │       └── filebeat.yaml
│   ├── elk-logstash
│   │   ├── docker-compose.yml
│   │   ├── elasticsearch
│   │   │   ├── data
│   │   │   ├── Dockerfile
│   │   │   └── elasticsearch.yml
│   │   ├── kibana
│   │   │   ├── Dockerfile
│   │   │   └── kibana.yml
│   │   ├── logstash
│   │   │   ├── Dockerfile
│   │   │   ├── logstash.conf
│   │   │   └── logstash.yml
│   │   └── nginx_template.json
│   ├── haproxy
│   │   └── haproxy.cfg
│   ├── nginx-blue
│   │   └── index.html
│   ├── nginx-green
│   │   └── index.html
│   ├── nginx-haproxy
│   │   └── docker-compose.yml
│   └── README.md
├── README.md
└── scripts
    ├── create_test_files.sh
    ├── README.md
    ├── replace_file_name.sh
    └── runtime_renew_file_name.sh
</pre>
