#!/bin/sh

#Pass the Argument ./runtime_renew_file_name.sh *.mp4

count=0
for filename in "$@"; do
    DATE=$(date +%Y_%m_%d_%H:%M:%S)
    echo "Current renaming file: $filename"
    mv "$filename" "videofile_${DATE}_${count}.mp4"
    count=`expr $count + 1`
    sleep 2s;
done
