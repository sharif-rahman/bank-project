Role Name
=========
Role Perfoms as follows
* use ansible to install postgresql on target server
* use ansible to ensure postgresql runs on server startup
* use ansible to enable inbound postgresql network connections from the network range 10.231.0.0/16 (md5)
* use ansible to ensure that SSH logins only work with ssh keypairs (no passwords) 
* use ansible to configure the firewall on the remote server to allow inbound connections on postgres port
* use ansible to configure a scheduled backup (dump) of the postgresql server

Requirements
------------

Role Variables
--------------


Dependencies
------------

Example Playbook
----------------

<pre>
   - name: Create a PostgreSQL DB server
      hosts: "{{myinstance}}"
      roles:
        - database

</pre>

Author Information
------------------
Sharif

Syntax
------
* Run the Docker Image with Ansible
  * docker run --detach --name=db_pg -v /etc/ansible/roles/database:/etc/ansible/roles/database/ --privileged -v /sys/fs/cgroup:/sys/fs/cgroup:ro geerlingguy/docker-ubuntu1804-ansible:latest

* Run the Playbook inside docker container
  * docker exec --tty db_pg env TERM=xterm ansible-playbook /etc/ansible/roles/database/tests/pg_db.yml -e "myinstance=localhost" -e "database_name=postgres" 
