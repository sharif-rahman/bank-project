# Information
2. DOCKER
Create a Docker-Compose multi-container application, that achieves the following goals:


* The html page should contain the word "blue" on one container and the word "green" on the other
  * nginx-blue
  * nginx-green


* place a haproxy loadbalancer container in front of nginx containers
  * Setup Loadbalancer: nginx-haproxy.

* enable a method to view haproxy statistics via web interface
  * Setup Loadbalancer: nginx-haproxy.

Bonus Tasks (optional)
* configure nginx access logs to be collected and stored in an ElasticSearch docker instance
  * elk-filebeat:
    * The setup of elasticsearch, kibana and filebeat.
    * The compose file is available inside the folder "docker-compose.yml"
  * elk-logstash
    * The setup of elasticsearch, kibana and logstash.
    * The compose file is available inside  the folder "docker-compose.yml"

* configure kibana to allow viewing these logs from ElasticSearch
  * elk-filebeat:
    * The setup of elasticsearch, kibana and filebeat.
    * The compose file is available inside the folder "docker-compose.yml"
  * elk-logstash
    * The setup of elasticsearch, kibana and logstash.
    * The compose file is available inside  the folder "docker-compose.yml"
