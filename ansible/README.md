Syntax
------
* Test ansible Playbook Using docker image

* Run the Docker Image with Ansible
  * docker run --detach --name=db_pg -v /etc/ansible/roles/database:/etc/ansible/roles/database/ --privileged -v /sys/fs/cgroup:/sys/fs/cgroup:ro geerlingguy/docker-ubuntu1804-ansible:latest

* Run the Playbook inside docker container
  * docker exec --tty db_pg env TERM=xterm ansible-playbook /etc/ansible/roles/database/tests/pg_db.yml -e "myinstance=localhost" -e "database_name=postgres"
