# Information 

* The script will create dummy files .wav / .mp4
  * create_test_files.sh

* The script will replace file name with data/time and count.
  * Syntax: ./replace_file_name.sh

* The script will replace the file name with data/time, count and suffix can provide on run time argument
  * Syntax: ./replace_file_name.sh *.mp4

